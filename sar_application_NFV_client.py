#!/usr/bin/python2

from scapy.all import *
import time 
from datetime import datetime
from threading import Thread


class MonitoringThread(Thread):
	def __init__(self, src, dst, tos, iface):
		Thread.__init__(self)
		self.src = src
		self.dst = dst
		self.tos = tos
		self.iface = iface

	def monitoring_app(self):
		while True:
			data = "Hi, I'm a client VNF"
			send(IP(src=self.src, dst=self.dst, tos=self.tos)/ICMP()/data) 
			time.sleep(1)

	def run(self):
		conf.L3socket = L3RawSocket
		self.monitoring_app()


thread = MonitoringThread("10.0.0.3", "10.0.0.2", 4, "enp0s3")
thread.start()

